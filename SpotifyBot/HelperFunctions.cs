﻿using System;

namespace SpotifyBot
{
    class HelperFunctions
    {
        static Random rand = new Random();
        public static double GetRandom()
        {
            return rand.NextDouble();
        }

        public static string FormatSongLength(int songLength)
        {
            TimeSpan span = TimeSpan.FromMilliseconds(songLength);
            return string.Format("{0:D1} minutes and {1:D2} seconds", span.Minutes, span.Seconds);
        }
    }
}
