﻿using System;
using System.Collections.Generic;
using System.Text;
using DSharpPlus.Entities;

namespace SpotifyBot
{
    class Player
    {
        public DiscordUser User { get; set; }
        public ulong DiscordID { get; set; }

        private int points;

        public Player(DiscordUser user)
        {
            User = user;
            DiscordID = User.Id;
        }

        public int GetPoints()
        {
            return points;
        }

        public void ResetPoints()
        {
            points = 0;
        }

        public void AddPoints()
        {
            points++;
        }

        public void AddPoints(int points)
        {
            this.points += points;
        }
    }
}
