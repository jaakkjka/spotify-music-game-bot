﻿using System;
using System.Collections.Generic;
using System.Text;
using RestSharp;
using RestSharp.Deserializers;

namespace SpotifyBot.Auth
{
    public class SpotifyToken
    {
        [DeserializeAs(Name = "access_token")]
        public string AccessToken { get; set; }
        [DeserializeAs(Name = "token_type")]
        public string TokenType { get; set; }
        [DeserializeAs(Name = "expires_in")]
        public int ExpiresIn { get; set; }
        [DeserializeAs(Name = "refresh_token")]
        public string RefreshToken { get; set; }
    }
}
