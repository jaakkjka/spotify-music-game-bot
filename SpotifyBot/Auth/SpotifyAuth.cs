﻿using System;
using System.Text;
using RestSharp;

namespace SpotifyBot.Auth
{
    public class SpotifyAuth
    {
        const string BaseUrl = "https://accounts.spotify.com";
        const string TokenURI = "/api/token";

        readonly string clientSecret = "SPOTIFY_CLIENT_SECRET_HERE";
        readonly string clientID = "SPOTIFY_CLIENT_ID_HERE";

        public SpotifyToken GetSpotifyToken()
        {
            RestClient restClient = new RestClient(BaseUrl);
            var request = new RestRequest(TokenURI, Method.POST);
            request.AddParameter("grant_type", "client_credentials");

            request.AddHeader("Authorization", $"Basic {Convert.ToBase64String(Encoding.ASCII.GetBytes($"{clientID}:{clientSecret}"))}");

            return restClient.Execute<SpotifyToken>(request).Data;
        }
    }
}
