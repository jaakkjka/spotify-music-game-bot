﻿using System;
using System.Collections.Generic;
using SpotifyBot.Attributes;

namespace SpotifyBot
{
    static class Rating
    {
        public static List<string> RatePlayerSong(Player player, Song song)
        {
            List<string> ratings = new List<string>();
            Challenge challenge = ChallengeHandler.CurrentChallenge;

            // Check each attribute in our challenge and compare it to the attributes that the player's song has
            foreach(BaseAttribute attribute in challenge.attributes)
            {
                var songAdjective = GetSongAttribute(song, attribute);
                bool isMatchingAdjectives = CheckMatchInAdjectives(songAdjective, attribute.GetAttributeAdjective());

                ratings.Add($"{attribute.RatingTextFirstPart} **{songAdjective}** {attribute.RatingTextSecondPart} **{attribute.GetAttributeAdjective()}**. ");

                if (isMatchingAdjectives)
                {
                    ratings.Add("Well done!");
                    player.AddPoints();
                    continue;
                }
                ratings.Add("Too bad!");
            }

            return ratings;
        }

        // Compare if the song adjective matches with the challenge adjective
        static bool CheckMatchInAdjectives(string songAdjective, string attributeAdjective)
        {
            if(songAdjective == attributeAdjective)
            {
                return true;
            }

            return false;
        }

        // Find the value of the desired attribute from the song
        // Return the adjective based on the value of the attribute
        static string GetSongAttribute(Song song, BaseAttribute a)
        {
            switch (a.Name)
            {
                case "Danceability":
                    if (song.Danceability > 0.5f)
                    {
                        return a.PositiveAdjective;
                    }
                    return a.NegativeAdjective;
                case "Energy":
                    if (song.Energy > 0.5f)
                    {
                        return a.PositiveAdjective;
                    }
                    return a.NegativeAdjective;
                case "Instrumentalness":
                    if (song.Instrumentalness > 0.5f)
                    {
                        return a.PositiveAdjective;
                    }
                    return a.NegativeAdjective;
                case "Tempo":
                    if (song.Tempo > a.Value)
                    {
                        return a.PositiveAdjective;
                    }
                    return a.NegativeAdjective;
                case "Valence":
                    if (song.Valence > 0.5f)
                    {
                        return a.PositiveAdjective;
                    }
                    return a.NegativeAdjective;
                default:
                    return "Error!";
            }
        }
    }
}
