﻿using System;
using System.Collections.Generic;

namespace SpotifyBot
{
    static class GameTracker
    {
        public static List<Player> players = new List<Player>();
        public static Song CurrentSong { get; set; }

        public static void AddNewPlayer(Player player)
        {
            players.Add(player);
            Console.WriteLine($"Player with ID {player.DiscordID} added successfully");
        }

        public static bool CheckIfPlayerExists(ulong id)
        {
            foreach(Player p in players)
            {
                if(p.DiscordID == id)
                {
                    return true;
                }
            }

            return false;
        }

        public static Player GetPlayer(ulong id)
        {
            foreach(Player p in players)
            {
                if (p.DiscordID == id)
                {
                    return p;
                }
            }

            return null;
        }
    }
}
