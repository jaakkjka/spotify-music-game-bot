﻿using System;
using System.Collections.Generic;
using System.Text;
using SpotifyBot.Attributes;

namespace SpotifyBot
{
    class Challenge
    {
        public List<BaseAttribute> attributes = new List<BaseAttribute>();

        public void Define(List<BaseAttribute> attributes)
        {
            this.attributes = attributes;
        }

        // Build the challenge as a string
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            
            for (int i = 0; i < attributes.Count; i++)
            {
                if (i == 0)
                {
                    sb.Append($"Give me a song {attributes[i].ChallengeText}");
                }

                else if (i > 0)
                {
                    sb.Append("and ");
                    sb.Append(attributes[i].ChallengeText);
                }

                if (attributes[i].IsPositive)
                {
                    sb.Append($" **{attributes[i].PositiveAdjective}** ");
                }

                else
                {
                    sb.Append($" **{attributes[i].NegativeAdjective}** ");
                }
            }

            return sb.ToString();
        }
    }
}
