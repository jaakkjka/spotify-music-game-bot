﻿using RestSharp;
using System;

namespace SpotifyBot
{
    class SpotifyTrackRequest
    {
        readonly string baseURI = "https://api.spotify.com/v1/audio-features";
        public Song GetSong(string spotifyURI, string accessToken)
        {
            var restClient = new RestClient(baseURI);
            var request = new RestRequest($"{baseURI}/{spotifyURI}", Method.GET);
            Console.WriteLine($"{baseURI}/{spotifyURI} with a access token {accessToken}");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader($"Authorization", $"Bearer {accessToken}");

            var response = restClient.Execute<Song>(request);
            if (response.ErrorException != null)
            {
                const string message = "Error retrieving response.  Check inner details for more info.";
                Console.WriteLine(message);
                Console.WriteLine(response.ErrorException);
            }
            return response.Data;
        }
    }
}