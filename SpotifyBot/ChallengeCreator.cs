﻿using SpotifyBot.Auth;
using SpotifyBot.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyBot
{
    class ChallengeCreator
    {
        List<BaseAttribute> allAttributes;
        List<BaseAttribute> selectedAttributes = new List<BaseAttribute>();

        // Get all attributes from which we randomize a couple for the actual challenge
        public void Initialize()
        {
            allAttributes = new List<BaseAttribute>
            {
                new Danceability(),
                new Energy(),
                new Valence(),
                new Instrumentalness(),
                new Tempo()
            };
        }

        public Challenge CreateNewChallenge()
        {
            selectedAttributes = InitializeSelectedAttributes();
            var challenge = new Challenge();
            ChallengeHandler.CurrentChallenge = challenge;
            ChallengeHandler.CurrentChallenge.Define(selectedAttributes);
            ChallengeHandler.ChallengeActive = true;

            return challenge;
        }

        // Randomize some attributes for the challenge.
        List<BaseAttribute> InitializeSelectedAttributes()
        {
            List<BaseAttribute> randomAttributes = new List<BaseAttribute>();
            Random rand = new Random();
            while(randomAttributes.Count == 0)
            {
                for (int i = 0; i < allAttributes.Count; i++)
                {
                    if (rand.NextDouble() < 0.3 || rand.NextDouble() > 0.7)
                    {
                        randomAttributes.Add(allAttributes[i]);

                    }
                }
            }

            return randomAttributes;
        }
    }
}
