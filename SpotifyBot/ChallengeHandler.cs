﻿
namespace SpotifyBot
{
    static class ChallengeHandler
    {
        public static Challenge CurrentChallenge { get; set; }
        public static bool ChallengeActive { get; set; }

        public static void ResetChallenge()
        {
            CurrentChallenge = null;
            ChallengeActive = false;
        }
    }
}
