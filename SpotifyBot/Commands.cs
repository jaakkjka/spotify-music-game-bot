﻿using System;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Interactivity;
using SpotifyBot.Auth;

namespace SpotifyBot
{
    class Commands
    {
        [Command("challenge")]
        public async Task SetNewChallenge(CommandContext context)
        {
            if (ChallengeHandler.ChallengeActive == false)
            {
                if (!GameTracker.CheckIfPlayerExists(context.User.Id))
                {
                    GameTracker.AddNewPlayer(new Player(context.User));
                    await context.RespondAsync($"👋 Hi, {context.User.Mention}! You were added to the game!");
                }

                // Creating the challenge for player
                var creator = new ChallengeCreator();
                creator.Initialize();
                var challenge = creator.CreateNewChallenge();
                string challengeText = challenge.ToString();
                await context.RespondAsync($"Your challenge is as follows:");
                // Small delay to make it look more natural
                await Task.Delay(1000);
                await context.RespondAsync(challengeText);
                await context.RespondAsync("Give me a Spotify URI link and I'll check it out!");
                await context.RespondAsync($"You have one minute starting now!");

                // Using Interactivity Module to wait for the response from player
                // TODO: Error handling for bad link
                var interactivity = context.Client.GetInteractivityModule();
                var msg = await interactivity.WaitForMessageAsync(xm => xm.Author.Id == context.User.Id && xm.Content.Split(':')[0] == "spotify", TimeSpan.FromMinutes(1));
                if (msg != null)
                {
                    // Get the song ID from the URI, Auth and make a request
                    var id = msg.Message.Content.Split(':')[2];
                    var str = new SpotifyTrackRequest();
                    var auth = new SpotifyAuth();
                    var song = str.GetSong(id, auth.GetSpotifyToken().AccessToken);
                    GameTracker.CurrentSong = song;
                    // Uncomment line below if you want to wait for the whole length of the song before getting ratings. For testing purposes the delay is now 5 sec.
                    // var h = HelperFunctions.FormatSongLength(song.Length);
                    await context.RespondAsync($"Gotcha, let's listen to https://open.spotify.com/track/{id}");
                    await context.RespondAsync($"See you in 30 seconds");
                    var rating = Rating.RatePlayerSong(GameTracker.GetPlayer(context.User.Id), song);
                    // 30 second delay, can be changed to song length
                    await Task.Delay(5000);
                    // Write the ratings for the player song
                    foreach (string s in rating)
                    {
                        await context.RespondAsync(s);
                        await Task.Delay(500);
                    }
                    ChallengeHandler.ResetChallenge();
                    await context.RespondAsync("Ready to go again!");
                }
                else
                {
                    Console.WriteLine("Message is null");
                    ChallengeHandler.ResetChallenge();
                    await context.RespondAsync("Missed your one minute deadline! Try again!");
                }
            } 
        }

        [Command("enter")]
        public async Task EnterCompetition(CommandContext context)
        {
            if (!GameTracker.CheckIfPlayerExists(context.User.Id))
            {
                GameTracker.AddNewPlayer(new Player(context.User));
                await context.RespondAsync($"👋 Hi, {context.User.Mention}! You were added to the game!");
            }

            else
            {
                await context.RespondAsync($"👋 Hi, {context.User.Mention}! You are already a part of the game!");
            }
        }

        [Command("points")]
        public async Task GetCurrentPoints(CommandContext context)
        {
            var player = GameTracker.GetPlayer(context.User.Id);
            await context.RespondAsync($"Hi {context.User.Mention}! Your points haul is {player.GetPoints()}!");
        }

        [Command("standings")]
        public async Task GetCurrentStandings(CommandContext context)
        {
            var players = GameTracker.players;
            await context.RespondAsync("Current standings:");
            foreach(Player p in players)
            {
                await context.RespondAsync($"{p.User.Username}: {p.GetPoints()} points");
            }
        }
        
        [Command("presence")]
        public async Task GetSpotifyStatus(CommandContext c)
        {
            var artist = c.User.Presence.Game.Details;
            var song = c.User.Presence.Game.State;
            await c.RespondAsync($"Presence: {artist} - {song}");
        }
    }
}
