﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyBot
{
    public class Song
    {
        [DeserializeAs(Name = "danceability")]
        public float Danceability { get; set; }
        [DeserializeAs(Name = "energy")]
        public float Energy { get; set; }
        [DeserializeAs(Name = "instrumentalness")]
        public float Instrumentalness { get; set; }
        [DeserializeAs(Name = "valence")]
        public float Valence { get; set; }
        [DeserializeAs(Name = "tempo")]
        public int Tempo { get; set; }
        [DeserializeAs(Name = "duration_ms")]
        public int Length { get; set; }
    }
}
