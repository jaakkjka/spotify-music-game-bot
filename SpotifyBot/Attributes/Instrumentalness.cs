﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyBot.Attributes
{
    class Instrumentalness : BaseAttribute
    {
        public Instrumentalness()
        {
            Name = "Instrumentalness";
            Value = new Random().NextDouble();
            PositiveAdjective = "Instrumental";
            NegativeAdjective = "Not Instrumental";
            SetPositivity();
        }

        public new void SetPositivity()
        {
            if (Value > 0.8d)
            {
                IsPositive = true;
            }
        }
    }
}
