﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyBot.Attributes
{
    class Danceability : BaseAttribute
    {
        public Danceability()
        {
            Name = "Danceability";
            Value = new Random().NextDouble();
            PositiveAdjective = "Danceable";
            NegativeAdjective = "Not so danceable";
        }
    }
}
