﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyBot.Attributes
{
    class BaseAttribute
    {
        public string Name { get; set; }
        public double Value { get; set; }
        public string PositiveAdjective { get; set; }
        public string NegativeAdjective { get; set; }
        public string NeutralAdjective { get; set; }
        public bool IsPositive { get; set; }
        public string ChallengeText { get; set; }
        public string RatingTextFirstPart { get; set; }
        public string RatingTextSecondPart { get; set; }

        public BaseAttribute()
        {
            ChallengeText = "that is";
            RatingTextFirstPart = "Your song was";
            RatingTextSecondPart = "and I wanted a song that was";
            SetPositivity();
        }

        public void SetPositivity()
        {
            if (Value > 0.5d)
            {
                IsPositive = true;
            }
        }

        public string GetAttributeAdjective()
        {
            if (IsPositive)
            {
                return PositiveAdjective;
            }

            return NegativeAdjective;
        }
    }
}
