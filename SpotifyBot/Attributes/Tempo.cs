﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyBot.Attributes
{
    class Tempo : BaseAttribute
    {
        public Tempo()
        {
            Name = "Tempo";
            Value = new Random().Next(70, 120);
            PositiveAdjective = $"over {Value}";
            NegativeAdjective = $"under {Value}";
            NeutralAdjective = "Tempo";
            ChallengeText = "that has a tempo of";
            RatingTextFirstPart = "Your song had a tempo of";
            RatingTextSecondPart = "and I wanted a song that had a tempo of";
        }
    }

}
