﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyBot.Attributes
{
    class Energy : BaseAttribute
    {
        public Energy()
        {
            Name = "Energy";
            Value = new Random().NextDouble();
            PositiveAdjective = "Energetic";
            NegativeAdjective = "Calm";
        }
    }
}
