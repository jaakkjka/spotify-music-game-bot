﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyBot.Attributes
{
    class Valence : BaseAttribute
    {
        public Valence()
        {
            Name = "Valence";
            Value = new Random().NextDouble();
            PositiveAdjective = "Happy";
            NegativeAdjective = "Sad";
        }
    }
}
