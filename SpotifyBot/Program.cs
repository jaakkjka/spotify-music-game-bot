﻿using System;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using System.Threading.Tasks;
using DSharpPlus.Interactivity;

namespace SpotifyBot
{
    class Program
    {
        static DiscordClient discord;
        static CommandsNextModule commands;
        static InteractivityModule interactivity;

        static void Main(string[] args)
        {
            MainAsync(args).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {
           
            // Connect to the bot
            discord = new DiscordClient(new DiscordConfiguration
            {
                Token = "DISCORD_BOT_TOKEN_HERE",
                TokenType = TokenType.Bot,
                UseInternalLogHandler = true,
                LogLevel = LogLevel.Debug
            });

            // Test for connection
            //discord.MessageCreated += async e =>
            //{
            //    if (e.Message.Content.ToLower().StartsWith("ping"))
            //        await e.Message.RespondAsync("pong!");
            //};

            // Defining a prefix for commands
            commands = discord.UseCommandsNext(new CommandsNextConfiguration
            {
                StringPrefix = "!"
            });

            commands.RegisterCommands<Commands>();

            var conf = new InteractivityConfiguration();
            interactivity = discord.UseInteractivity(conf);
            ChallengeHandler.ChallengeActive = false;

            await discord.ConnectAsync();
            // Prevent the bot from disconnecting
            await Task.Delay(-1);
        }
    }
}
